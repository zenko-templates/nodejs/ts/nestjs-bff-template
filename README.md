# Zenko's Nestjs Backend For Frontend Template (to be replaced with <my_project> name)

## Description

Made with: [**NestJS**](https://docs.nestjs.com)

Graphql api for frontend

Add project description

## Project Resources

* [Schema](./docs/schemas)


## How to use this template

```bash
# Clone the repository
git clone git@gitlab.com:zenko-templates/nodejs/ts/nestjs-bff-template.git <my_project>

# Delete the git template's data
cd <my_project>
rm -rf .git
```

Then configure your new project accordingly

This project come with a mongoose connection for prototyping.  
If you don't have a use of it don't forget to remove it :

* [ ] `src/main.ts`: remove the mongodb connection
* [ ] `src/config/dto/config.dto.ts`: remove the MONGODB_URI config variable
* [ ] `src/config/default-config.ts`: remove the DEFAULT_MONGODB_URI default variable


### Update Api name and version

* [ ] `package.json`: change `name`, `version` and `description` attributes

### Finalize bootstrap

```bash
# Initialize your new repository
git init
git remote add origin git@gitlab.com:<my_project>.git
git commit -a -m "Initial commit"
git push -u origin master
```

## Installation

Here some documentations about project setup and configuration

### Requirements

* [NodeJS v17.4.0+](https://nodejs.org/en/blog/release/v17.4.0/)
* [NPM v8](https://www.npmjs.com/package/npm)

### Environment variables

First copy `.env.dist` to provide local `.env` file

```bash
$ cp .env.dist .env
```

Update environment variables values if necessary

This project validate expecting format for every environment variables

#### Adding new variable

* Setup default value when necessary on [`./src/config/default-config.ts`](./src/config/default-config.ts)
* Introduce new environment variables on [`./src/config/dto/config.dto.ts`](./src/config/dto/config.dto.ts)
* Don't forget to add it on [`.env.dist`](./.env.dist)

### Generate typescript type for graphql

```bash
$ npm run generate:graphql
```

### Install

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
