import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['./docs/schemas/**/*.graphql'],
  path: join(process.cwd(), 'src/graphql.interface.ts'),
  watch: true,
  defaultScalarType: 'unknown',
  customScalarTypeMapping: {
    DateTime: 'Date',
    Cursor: 'string',
  },
});
