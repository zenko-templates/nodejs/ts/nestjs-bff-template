
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface CategoriesInput {
    page: PageInput;
}

export interface CreateCategoryInput {
    name: string;
}

export interface UpdateCategoryInput {
    id: string;
    name?: Nullable<string>;
}

export interface PageInput {
    first?: Nullable<number>;
    after?: Nullable<Cursor>;
    last?: Nullable<number>;
    before?: Nullable<Cursor>;
}

export interface PeriodSearchInput {
    start?: Nullable<DateTime>;
    end?: Nullable<DateTime>;
}

export interface FilterProductInput {
    categoryId?: Nullable<string>;
}

export interface ProductsInput {
    page: PageInput;
    filter: FilterProductInput;
}

export interface CreateProductInput {
    name: string;
    categoryId: string;
}

export interface UpdateProductInput {
    id: string;
    name?: Nullable<string>;
    categoryId: string;
}

export interface IMutation {
    createCategory(input: CreateCategoryInput): Category | Promise<Category>;
    updateCategory(input: UpdateCategoryInput): Category | Promise<Category>;
    deleteCategory(id: string): Category | Promise<Category>;
    createProduct(input: CreateProductInput): Product | Promise<Product>;
    updateProduct(input: UpdateProductInput): Product | Promise<Product>;
    deleteProduct(id: string): Product | Promise<Product>;
}

export interface IQuery {
    categories(input: CategoriesInput): CategoryConnection | Promise<CategoryConnection>;
    category(id: string): Nullable<Category> | Promise<Nullable<Category>>;
    products(input: ProductsInput): ProductConnection | Promise<ProductConnection>;
    product(id: string): Nullable<Product> | Promise<Nullable<Product>>;
}

export interface Category {
    id: string;
    name: string;
    products: Product[];
}

export interface CategoryEdge {
    node: Category;
    cursor: Cursor;
}

export interface CategoryConnection {
    edges: CategoryEdge[];
    pageInfo: PageInfo;
}

export interface PageInfo {
    endCursor?: Nullable<Cursor>;
    hasNextPage: boolean;
    startCursor?: Nullable<Cursor>;
    hasPreviousPage: boolean;
}

export interface Product {
    id: string;
    name: string;
    category: Category;
}

export interface ProductEdge {
    node: Product;
    cursor: Cursor;
}

export interface ProductConnection {
    edges: ProductEdge[];
    pageInfo: PageInfo;
}

export type DateTime = Date;
export type Cursor = string;
type Nullable<T> = T | null;
