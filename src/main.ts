import { INestApplication, Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';

import { AppModule } from './app.module';
import { EnvironmentVariablesDto } from './config/dto/config.dto';

import { name, version } from '../package.json';

async function bootstrap() {
  Logger.log(`Starting ${name} (v${version})...`);

  const app = await NestFactory.create<INestApplication>(AppModule);

  const configService =
    app.get<ConfigService<EnvironmentVariablesDto>>(ConfigService);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  const port = configService.get('PORT');
  const host = configService.get('HOST');

  await app.listen(port, host);

  Logger.log(`Listening on ${host}:${port}`);
}
bootstrap();
