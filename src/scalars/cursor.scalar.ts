import { Scalar, CustomScalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';
import {encodeBase64} from "../utils/encode-base64";
import {decodeBase64} from "../utils/decode-base64";

@Scalar('Cursor')
export class CursorScalar implements CustomScalar<string, string> {
  description = 'Custom Cursor scalar type';

  parseValue(value: string): string {
    return decodeBase64(value);
  }

  serialize(value: string): string {
    return encodeBase64(value);
  }

  parseLiteral(ast: ValueNode): string {
    if (ast.kind === Kind.STRING) {
      return decodeBase64(ast.value);
    }
    return null;
  }
}
