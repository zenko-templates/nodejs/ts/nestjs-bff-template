import { Module } from '@nestjs/common';
import { DateScalar } from './date-time.scalar';
import {CursorScalar} from "./cursor.scalar";

@Module({
  providers: [CursorScalar, DateScalar],
})
export class ScalarModule {}
