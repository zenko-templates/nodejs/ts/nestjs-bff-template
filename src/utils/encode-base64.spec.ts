import {encodeBase64} from "./encode-base64";


describe('Encode to base 64', () => {
	it('encode a string to a base 64', () => {
		const pageInfo = encodeBase64("6242df4566c229c84c45e414");
		expect(pageInfo).toEqual("NjI0MmRmNDU2NmMyMjljODRjNDVlNDE0");
	});
});
