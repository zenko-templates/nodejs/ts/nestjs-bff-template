import {decodeBase64} from "./decode-base64";


describe('Decode from base 64', () => {
	it('decode a base 64 to a string', () => {
		const pageInfo = decodeBase64("NjI0MmRmNDU2NmMyMjljODRjNDVlNDE0");
		expect(pageInfo).toEqual("6242df4566c229c84c45e414");
	});
});
