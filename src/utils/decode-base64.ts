export function decodeBase64(s: string): string {
	const buff = Buffer.from(s, 'base64');
	return buff.toString();
}