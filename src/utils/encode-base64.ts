export function encodeBase64(s: string): string {
	const buff = Buffer.from(s);
	return buff.toString('base64');
}