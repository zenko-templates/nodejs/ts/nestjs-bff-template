import {Document, FilterQuery} from "mongoose";

export interface PaginationOption<T extends Document> {
	filter: FilterQuery<T>;
	limit: number;
}