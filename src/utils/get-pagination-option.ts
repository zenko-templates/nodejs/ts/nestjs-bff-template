import {Document, FilterQuery} from "mongoose";
import {PaginationOption} from "./interfaces/pagination.interface";
import {PageInput} from "../graphql.interface";

export function getPaginationOption<T extends Document>(page: PageInput, otherFilter?: FilterQuery<T>): PaginationOption<T> {
	let filter: FilterQuery<T>;
	let limit = page.first;

	if (page.before) {
		filter = { ...otherFilter, _id: { $lt: page.before }};
		limit = page.last;
	} else if (page.after) {
		filter = { ...otherFilter, _id: { $gt: page.after }};
		limit = page.first;
	}


	return {
		filter,
		limit,
	}
}