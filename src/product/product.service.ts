import {forwardRef, Inject, Injectable, Logger, NotFoundException} from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateProductInput, ProductsInput, UpdateProductInput } from "../graphql.interface";
import { FilterQuery } from 'mongoose';
import {ProductDocument, ProductEntity} from "./entities/product.entity";
import {InjectModel} from "@nestjs/mongoose";
import {CategoryDocument} from "../category/entities/category.entity";
import {CategoryService} from "../category/category.service";
import {getPaginationOption} from "../utils/get-pagination-option";

@Injectable()
export class ProductService {

  private logger: Logger;

  constructor(
      @InjectModel(ProductEntity.name) private readonly productModel: Model<ProductDocument>,
      @Inject(forwardRef(() => CategoryService)) private readonly categoryService: CategoryService
  ) {}

  async create(input: CreateProductInput): Promise<ProductDocument> {

    if (!await this.categoryService.exist(input.categoryId)) {
      throw new NotFoundException(`Category: ${input.categoryId} was not found.`)
    }

    const product = new this.productModel({ ...input });
    return product.save();
  }

  async search(input: ProductsInput): Promise<ProductDocument[]> {

    let filter: FilterQuery<ProductDocument> = {
      ...(input.filter.categoryId && {
        categoryId: input.filter.categoryId
      }),
    };

    const paginationOption = getPaginationOption(input.page, filter)

    const query = this.productModel
        .find(paginationOption.filter)
        .sort({});

    if (paginationOption.limit !== -1) {
      return query.limit(paginationOption.limit);
    }
    return query;
  }

  async findById(id: string): Promise<ProductDocument> {
    const product = await this.productModel.findById(id);

    if (!product) {
      this.logger.error(`Product: ${id} was not found`)
      throw new NotFoundException(`Product: ${id} was not found`)
    }

    return product;
  }

  async update(id: string, input: UpdateProductInput): Promise<ProductDocument> {
    if (input.categoryId !== "" && !await this.categoryService.exist(input.categoryId)) {
      throw new NotFoundException(`Category: ${input.categoryId} was not found.`)
    }

    const product = await this.productModel.findByIdAndUpdate(id, { ...input }, { new: true });

    if (!product) {
      this.logger.error(`Product: ${id} was not found`)
      throw new NotFoundException(`Product: ${id} was not found`)
    }

    return product;
  }

  async remove(id: string): Promise<ProductDocument> {
    const product = await this.productModel.findByIdAndDelete(id);

    if (!product) {
      this.logger.error(`Product: ${id} was not found`)
      throw new NotFoundException(`Product: ${id} was not found`)
    }

    return product;
  }

  async exist(id: string): Promise<Boolean> {
    const product = await this.productModel.exists({ _id: id });

    return !!product;
  }

  async hasNext(id: string): Promise<boolean> {
    const product = await this.productModel.findOne({ _id: { $gt: id }});

    return !!product;
  }

  async hasPrevious(id: string): Promise<boolean> {
    const product = await this.productModel.findOne({ _id: { $lt: id }});

    return !!product;
  }

  async category(categoryId: string): Promise<CategoryDocument> {
    return this.categoryService.findById(categoryId);
  }
}
