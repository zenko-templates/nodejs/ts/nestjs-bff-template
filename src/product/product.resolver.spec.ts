import { Test, TestingModule } from '@nestjs/testing';
import { ProductResolver } from './product.resolver';
import { ProductService } from './product.service';
import {ProductMappers} from "./mappers/product.mappers";
import {CategoryMappers} from "../category/mappers/category.mappers";
jest.mock('./product.service');
jest.mock('./mappers/product.mappers');
jest.mock('../category/mappers/category.mappers');

describe('ProductResolver', () => {
  let resolver: ProductResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductResolver, ProductService, ProductMappers, CategoryMappers],
    }).compile();

    resolver = module.get<ProductResolver>(ProductResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
