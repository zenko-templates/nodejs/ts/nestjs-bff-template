import { Injectable } from '@nestjs/common';
import {ProductDocument} from "../entities/product.entity";
import {Category, Product, ProductConnection} from "../../graphql.interface";

@Injectable()
export class ProductMappers {
	public toGraphql(element: ProductDocument): Product {
		return {
			id: element.id,
			name: element.name,
			category: {
				id: element.categoryId,
			} as Category
		}
	}

	public toGraphqlConnection(elements: ProductDocument[], hasPreviousPage: boolean = false, hasNextPage: boolean = false): ProductConnection {
		return {
			edges: elements.map((product) => ({
				node: this.toGraphql(product),
				cursor: product.id
			})),
			pageInfo: {
				startCursor: elements.at(0)?.id,
				hasPreviousPage: hasPreviousPage,
				endCursor: elements.at(-1)?.id,
				hasNextPage: hasNextPage,
			}
		}
	}
}
