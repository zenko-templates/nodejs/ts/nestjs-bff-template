import { Test, TestingModule } from '@nestjs/testing';
import { ProductMappers } from './product.mappers';

describe('Mappers', () => {
  let provider: ProductMappers;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductMappers],
    }).compile();

    provider = module.get<ProductMappers>(ProductMappers);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
