import {forwardRef, Module} from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductResolver } from './product.resolver';
import {MongooseModule} from "@nestjs/mongoose";
import {ProductEntity, ProductSchema} from "./entities/product.entity";
import {CategoryModule} from "../category/category.module";
import { ProductMappers } from './mappers/product.mappers';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: ProductEntity.name, schema: ProductSchema}]),
    forwardRef(() => CategoryModule)
  ],
  providers: [ProductResolver, ProductService, ProductMappers],
  exports: [ProductService, ProductMappers]
})
export class ProductModule {}
