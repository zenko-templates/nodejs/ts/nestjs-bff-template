import { Test, TestingModule } from '@nestjs/testing';
import { ProductService } from './product.service';
import {getModelToken} from "@nestjs/mongoose";
import {ProductEntity} from "./entities/product.entity";
import {CategoryService} from "../category/category.service";
jest.mock("../category/category.service");

describe('ProductService', () => {
  let service: ProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductService,
        CategoryService,
        {
          provide: getModelToken(ProductEntity.name),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ProductService>(ProductService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
