import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose'

export type ProductDocument = ProductEntity & Document;

@Schema({ collection: 'product' })
export class ProductEntity {
	@Prop({ required: true })
	name: string;

	@Prop({ required: true })
	categoryId: string;
}

export const ProductSchema = SchemaFactory.createForClass(ProductEntity);
