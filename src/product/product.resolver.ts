import {Resolver, Query, Mutation, Args, ResolveField, Parent} from '@nestjs/graphql';
import { ProductService } from './product.service';
import {
  Category,
  CreateProductInput,
  Product, ProductConnection,
  ProductsInput,
  UpdateProductInput
} from "../graphql.interface";
import {ProductMappers} from "./mappers/product.mappers";
import {CategoryMappers} from "../category/mappers/category.mappers";

@Resolver('Product')
export class ProductResolver {
  constructor(
      private readonly productService: ProductService,
      private readonly productMappers: ProductMappers,
      private readonly categoryMappers: CategoryMappers,
  ) {}

  @Mutation('createProduct')
  async create(@Args('input') input: CreateProductInput): Promise<Product> {
    const product = await this.productService.create(input);

    return this.productMappers.toGraphql(product);
  }

  @Query('products')
  async findAll(@Args('input') input: ProductsInput): Promise<ProductConnection> {
    const products = await this.productService.search(input);
    const hasPreviousPage = await this.productService.hasPrevious(products.at(0)?.id)
    const hasNextPage = await this.productService.hasNext(products.at(0)?.id)

    return this.productMappers.toGraphqlConnection(products, hasPreviousPage, hasNextPage)
  }

  @Query('product')
  async findOne(@Args('id') id: string): Promise<Product> {
    const product = await this.productService.findById(id);

    return this.productMappers.toGraphql(product);
  }

  @Mutation('updateProduct')
  async update(@Args('input') input: UpdateProductInput): Promise<Product> {
    const product = await this.productService.update(input.id, input);

    return this.productMappers.toGraphql(product);
  }

  @Mutation('deleteProduct')
  async remove(@Args('id') id: string): Promise<Product> {
    const product = await this.productService.remove(id);

    return this.productMappers.toGraphql(product);
  }

  @ResolveField('category')
  async category(@Parent() product: Product): Promise<Category> {
    const category = await this.productService.category(product.category.id);

    return this.categoryMappers.toGraphql(category);
  }
}
