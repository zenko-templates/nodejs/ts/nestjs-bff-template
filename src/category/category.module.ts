import {forwardRef, Module} from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryResolver } from './category.resolver';
import {MongooseModule} from "@nestjs/mongoose";
import {CategoryEntity, CategorySchema} from "./entities/category.entity";
import {ProductModule} from "../product/product.module";
import {CategoryMappers} from "./mappers/category.mappers";

@Module({
  imports: [
      MongooseModule.forFeature([{ name: CategoryEntity.name, schema: CategorySchema}]),
      forwardRef(() => ProductModule)
  ],
  providers: [CategoryResolver, CategoryService, CategoryMappers],
  exports: [CategoryService, CategoryMappers]
})
export class CategoryModule {}
