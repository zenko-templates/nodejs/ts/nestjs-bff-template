import { Injectable } from '@nestjs/common';
import {CategoryDocument} from "../entities/category.entity";
import {Category, CategoryConnection} from "../../graphql.interface";

@Injectable()
export class CategoryMappers {
	public toGraphql(element: CategoryDocument): Category {
		return {
			id: element.id,
			name: element.name,
			products: []
		}
	}

	public toGraphqlConnection(elements: CategoryDocument[], hasPreviousPage: boolean = false, hasNextPage: boolean = false): CategoryConnection {
		return {
			edges: elements.map((category) => ({
				node: this.toGraphql(category),
				cursor: category.id
			})),
			pageInfo: {
				startCursor: elements.at(0)?.id,
				hasPreviousPage: hasPreviousPage,
				endCursor: elements.at(-1)?.id,
				hasNextPage: hasNextPage,
			}
		}
	}
}
