import { Test, TestingModule } from '@nestjs/testing';
import { CategoryMappers } from './category.mappers';

describe('Mappers', () => {
  let provider: CategoryMappers;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CategoryMappers],
    }).compile();

    provider = module.get<CategoryMappers>(CategoryMappers);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
