import {forwardRef, Inject, Injectable, Logger, NotFoundException} from '@nestjs/common';
import { Model } from 'mongoose';
import {CreateCategoryInput, CategoriesInput, UpdateCategoryInput} from "../graphql.interface";
import { FilterQuery } from 'mongoose';
import {CategoryDocument, CategoryEntity} from "./entities/category.entity";
import {InjectModel} from "@nestjs/mongoose";
import {ProductDocument} from "../product/entities/product.entity";
import {ProductService} from "../product/product.service";
import {getPaginationOption} from "../utils/get-pagination-option";

@Injectable()
export class CategoryService {

  private logger: Logger;

  constructor(
      @InjectModel(CategoryEntity.name) private readonly categoryModel: Model<CategoryDocument>,
      @Inject(forwardRef(() => ProductService)) private readonly productService: ProductService
  ) {}

  async create(input: CreateCategoryInput): Promise<CategoryDocument> {
    const category = new this.categoryModel({ ...input });
    return category.save();
  }

  async search(input: CategoriesInput): Promise<CategoryDocument[]> {

    let filter: FilterQuery<CategoryDocument> = {};

    const paginationOption = getPaginationOption(input.page, filter);

    const query = this.categoryModel
        .find(paginationOption.filter)
        .sort({});

    if (paginationOption.limit !== -1) {
      return query.limit(paginationOption.limit);
    }
    return query;
  }

  async findById(id: string): Promise<CategoryDocument> {
    const category = await this.categoryModel.findById(id);

    if (!category) {
      this.logger.error(`Category: ${id} was not found`)
      throw new NotFoundException(`Category: ${id} was not found`)
    }

    return category;
  }

  async update(id: string, input: UpdateCategoryInput): Promise<CategoryDocument> {
    const category = await this.categoryModel.findByIdAndUpdate(id, { ...input }, { new: true });

    if (!category) {
      this.logger.error(`Category: ${id} was not found`)
      throw new NotFoundException(`Category: ${id} was not found`)
    }

    return category;
  }

  async remove(id: string): Promise<CategoryDocument> {
    const category = await this.categoryModel.findByIdAndDelete(id);

    if (!category) {
      this.logger.error(`Category: ${id} was not found`)
      throw new NotFoundException(`Category: ${id} was not found`)
    }

    return category;
  }

  async exist(id: string): Promise<Boolean> {
    const category = await this.categoryModel.exists({ _id: id });

    return !!category;
  }

  async hasNext(id: string): Promise<boolean> {
    const category = await this.categoryModel.findOne({ _id: { $gt: id }});

    return !!category;
  }

  async hasPrevious(id: string): Promise<boolean> {
    const category = await this.categoryModel.findOne({ _id: { $lt: id }});

    return !!category;
  }

  async products(categoryId: string): Promise<ProductDocument[]> {
    return this.productService.search({
      page: { first: -1 },
      filter: { categoryId }
    });
  }
}
