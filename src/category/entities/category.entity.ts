import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose'

export type CategoryDocument = CategoryEntity & Document;

@Schema({ collection: 'category' })
export class CategoryEntity {
	@Prop({ required: true })
	name: string;
}

export const CategorySchema = SchemaFactory.createForClass(CategoryEntity);
