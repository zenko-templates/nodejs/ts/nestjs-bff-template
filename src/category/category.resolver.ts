import {Resolver, Query, Mutation, Args, ResolveField, Parent} from '@nestjs/graphql';
import { CategoryService } from './category.service';
import {
  CreateCategoryInput,
  Category,
  UpdateCategoryInput,
  CategoriesInput, Product, CategoryConnection
} from "../graphql.interface";
import {CategoryMappers} from "./mappers/category.mappers";
import {ProductMappers} from "../product/mappers/product.mappers";

@Resolver('Category')
export class CategoryResolver {
  constructor(
      private readonly categoryService: CategoryService,
      private readonly categoryMappers: CategoryMappers,
      private readonly productMappers: ProductMappers,
  ) {}

  @Mutation('createCategory')
  async create(@Args('input') input: CreateCategoryInput): Promise<Category> {
    const category = await this.categoryService.create(input);

    return this.categoryMappers.toGraphql(category);
  }

  @Query('categories')
  async findAll(@Args('input') input: CategoriesInput): Promise<CategoryConnection> {
    const categories = await this.categoryService.search(input);
    const hasPreviousPage = await this.categoryService.hasPrevious(categories.at(0)?.id);
    const hasNextPage = await this.categoryService.hasNext(categories.at(0)?.id);

    return this.categoryMappers.toGraphqlConnection(categories, hasPreviousPage, hasNextPage);
  }

  @Query('category')
  async findOne(@Args('id') id: string): Promise<Category> {
    const category = await this.categoryService.findById(id);

    return this.categoryMappers.toGraphql(category);
  }

  @Mutation('updateCategory')
  async update(@Args('input') input: UpdateCategoryInput): Promise<Category> {
    const category = await this.categoryService.update(input.id, input);

    return this.categoryMappers.toGraphql(category);
  }

  @Mutation('deleteCategory')
  async remove(@Args('id') id: string): Promise<Category> {
    const category = await this.categoryService.remove(id);

    return this.categoryMappers.toGraphql(category);
  }

  @ResolveField('products')
  async products(@Parent() category: Category): Promise<Product[]> {
    const products = await this.categoryService.products(category.id);

    return products.map((product) => this.productMappers.toGraphql(product))
  }
}
