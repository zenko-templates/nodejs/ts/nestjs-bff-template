import { Test, TestingModule } from '@nestjs/testing';
import { CategoryService } from './category.service';
import {getModelToken} from "@nestjs/mongoose";
import {CategoryEntity} from "./entities/category.entity";
import {ProductService} from "../product/product.service";
jest.mock('../product/product.service');

describe('CategoryService', () => {
  let service: CategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoryService,
          ProductService,
        {
          provide: getModelToken(CategoryEntity.name),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CategoryService>(CategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
