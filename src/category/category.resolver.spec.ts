import { Test, TestingModule } from '@nestjs/testing';
import { CategoryResolver } from './category.resolver';
import { CategoryService } from './category.service';
import {CategoryMappers} from "./mappers/category.mappers";
import {ProductMappers} from "../product/mappers/product.mappers";
jest.mock('./category.service');
jest.mock('./mappers/category.mappers');
jest.mock('../product/mappers/product.mappers');

describe('CategoryResolver', () => {
  let resolver: CategoryResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CategoryResolver, CategoryService, CategoryMappers, ProductMappers],
    }).compile();

    resolver = module.get<CategoryResolver>(CategoryResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
