import { Module } from '@nestjs/common';
import {ConfigModule, ConfigService} from '@nestjs/config';

import { validate } from './config/validate-environment-variables';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ScalarModule } from './scalars/scalar.module';
import { ProductModule } from './product/product.module';
import {EnvironmentVariablesDto} from "./config/dto/config.dto";
import {MongooseModule, MongooseModuleOptions} from "@nestjs/mongoose";

const configModule = ConfigModule.forRoot({ isGlobal: true, validate })

@Module({
  imports: [
    configModule,
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      debug: true,
      playground: true,
      typePaths: ['./docs/schemas/**/*.graphql'],
    }),
    // Database uses for prototyping only in the template
    // If you don't have any uses of it remove it
    // and remove the config variable MONGODB_URI in both config/dto/config.dto.ts and config/default-config.ts
    MongooseModule.forRootAsync({
      imports: [configModule],
      useFactory: async (
          configService: ConfigService<EnvironmentVariablesDto>,
      ): Promise<MongooseModuleOptions> => ({
        uri: configService.get<string>('MONGODB_URI'),
        useNewUrlParser: true,
      }),
      inject: [ConfigService],
    }),
    ScalarModule,
    ProductModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
