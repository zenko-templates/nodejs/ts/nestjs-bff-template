import { Type } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsString, IsNumber } from 'class-validator';

import * as BASE_CONFIG from '../default-config';
import { AvailableNodeEnvironments } from '../enums/node-environments.enum';

export class EnvironmentVariablesDto {
  @IsString()
  @IsNotEmpty()
  HOST = BASE_CONFIG.DEFAULT_HOST;

  @IsEnum(AvailableNodeEnvironments)
  NODE_ENV = BASE_CONFIG.DEFAULT_NODE_ENV;

  @IsNumber()
  @Type(() => Number)
  PORT = BASE_CONFIG.DEFAULT_PORT;

  @IsString()
  @IsNotEmpty()
  MONGODB_URI = BASE_CONFIG.DEFAULT_MONGODB_URI;
}
