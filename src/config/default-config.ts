import { AvailableNodeEnvironments } from './enums/node-environments.enum';

// Add default environment variables value here when necessary
export const DEFAULT_HOST = '127.0.0.1';
export const DEFAULT_NODE_ENV = AvailableNodeEnvironments.development;
export const DEFAULT_PORT = 8080;

export const DEFAULT_MONGODB_URI = '';
