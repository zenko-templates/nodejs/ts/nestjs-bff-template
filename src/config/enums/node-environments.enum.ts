export enum AvailableNodeEnvironments {
  development = 'development',
  production = 'production',
  test = 'test',
}
